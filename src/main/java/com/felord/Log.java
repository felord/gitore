package com.felord;

/**
 * Created with IntelliJ IDEA.
 * Author: Dax
 * Description:
 * Date: 2018/3/29
 * Time: 14:26
 */


public @interface Log {
    String value() default "";
    LogType type();
}
