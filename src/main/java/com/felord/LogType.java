package com.felord;

/**
 * @author Dax
 * @version v1.0
 * @date 2018/3/29 14:32
 */
public enum LogType {
    DAO, SERVICE, ACTION
}
