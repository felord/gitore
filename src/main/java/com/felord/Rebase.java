package com.felord;

/**
 * The type Rebase.
 *
 * @author Dax
 * @version release
 * @date 2018 -03-29 17:40
 */
public class Rebase {

    private String name;
    private int code;
    private double lique;

    private Rebase(String name, int code, double lique) {
        this.name = name;
        this.code = code;
        this.lique = lique;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }


    /**
     * Gets code.
     *
     * @return the code
     */
    public int getCode() {
        return code;
    }


    /**
     * Gets lique.
     *
     * @return the lique
     */
    public double getLique() {
        return lique;
    }

    /**
     * The type Builder.
     */
    public static class Builder {
        private String name;
        private int code;
        private double lique;

        /**
         * Instantiates a new Builder.
         *
         * @param name the name
         */
        public Builder(String name) {
            this.name = name;
        }

        /**
         * Code builder.
         *
         * @param code the code
         * @return the builder
         */
        public Builder code(int code) {
            this.code=code;
            return this;
        }

        /**
         * Lique builder.
         *
         * @param lique the lique
         * @return the builder
         */
        public Builder lique(double lique) {
            this.lique=lique;
            return this;
        }

        public Rebase build() {
            return new Rebase(name, code, lique);
        }
    }

    @Override
    public String toString() {
        return "{" +
                "name:" + name +
                ", code:" + code +
                ", lique:" + lique +
                '}';
    }
}
