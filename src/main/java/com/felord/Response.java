package com.felord;

/**
 * Created with IntelliJ IDEA.
 * Author: Dax
 * Description:
 * Date: 2018/3/26
 * Time: 14:28
 */


public interface Response<T> {

    T getReturnEntity();
}
